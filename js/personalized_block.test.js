(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.personalizedBlockTest = {
    attach: function (context) {
      // Find the personalized block on the page.
      $('.personalized-block', context).once('personalized-block-test').each(function () {
        var $personalizedBlock = $(this);
        var personalizedContext = $personalizedBlock.data('context');
        var personalizedId = $personalizedBlock.data('id');

        // DEBUG: Output personalized information.
        console.log('Found Personalized Block: ' + personalizedContext + '/' + personalizedId)
        var personalizedApi = $personalizedBlock.data('api');
        if (personalizedApi) {
          console.log('API: ' + $personalizedBlock.data('api'));
        }

        // DEBUG: Output test URLs.
        $($('.personalized-block-content[data-id]', $personalizedBlock).each(function () {
          var variantId = $(this).data('id');
          var url = window.location.href.split('?')[0];
          console.log('Test ' + personalizedContext + '/' + personalizedId + '/' + variantId + ': ' + url + '?_variant=' + encodeURIComponent(variantId));
        }));

        // Request the variant to be displayed from our test personalization
        // engine.
        var url = drupalSettings.path.baseUrl + 'personalized-block/test';
        var data = {
          context: personalizedContext,
          id: personalizedId,
          location: window.location.toString(),
        };
        $.get(url, data, function getVariant(response) {
          var variantId = response.variant_id;
          if (!variantId) {
            return;
          }

          var $variant = $personalizedBlock.find('[data-id="' + variantId + '"]');
          if (!$variant.length) {
            return;
          }

          // Hide the default HTML and show variant HTML
          $personalizedBlock.find('[data-id="default"]').hide();
          $variant.show();

          // DEBUG: Output variant information.
          console.log('Found Personalized Block Variant : ' + personalizedContext + '/' + personalizedId + '/' + variantId)
          var variantApi = $variant.data('api');
          if (variantApi) {
            console.log('API ' + personalizedContext + '/' + personalizedId + '/' + variantId + ': ' + $variant.data('api'));
          }
        });
      });
    }
  };

} (jQuery, Drupal, drupalSettings));
