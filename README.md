# Personalized Block module

<https://www.drupal.org/sandbox/bronzehedwick/3227437>


## DONE

- Create Pesonalized contexts (personalized_contexts) vocabulary - DONE

- Add personalized_context field to personalized HTML content block. - DONE

- Create personlized_block_demo.module - DONE

- Remove \Drupal\personalized_block\Plugin\Block\PersonalizedBlock - DONE

- Add personalized_block.js which hide/shows variants based on querystring parameters - DONE

- In the demo module add context to taxanomy and the block - DONE

- Prevent personalized block module from being installed if the bartik theme is not enabled and the default theme DONE

- Remove default HTML and add default variant - DONE

- Rename field_personalized_contexts to be singular - DONE

- Move field value creation into helper functions - DONE

- Change Name to Content ID. - DONE

- Rename field_personalized_html to field_personalized_html - DONE

- Move entity creation into personalized_block_demo_create_entiity - DONE

- Clean up config dependency to only require root configuration - DONE

- Refactor block content view alter to be useable for other block types - DONE

- Add JSON:API to demo attributes - DONE

- Refactor personalized_block_demo.install to support MSKCC demo - DONE

- Add block field configuration - DONE

- Add block field support example - DONE

- Setup demo of block field - DONE

- Setup block field display - DONE

- Refactor personalized_block_block_content_view_alter to be reusable. DONE

- Improve JS reuse. - DONE

- Rename field_personalized_html_variants to field_personalized_html - DONE

- Add hook_form_alter() and remove the 'Display title' checkboxes from configuration form. - DONE

- Add field_personalized_id field to block content types. - DONE

- Add personalized_block_demo.css and library. DONE

- Review UI/UX add descriptions DONE

- Create personalized_block.test.js with Drupal.behaviors.personalizedBlockTest behavior DONE

- Create personalized_paragraph block content type - DONE

- Create personalized_paragraph block demo - DONE

- Alter the block_content edit form's info textfield label and description. - DONE

- Add 'Personalize' route, tab, and contextual links - DONE

## Known Issues

- Notice when uninstalling the demo.
  [notice] The HTML Variant bundle (entity type: paragraph) was deleted.

## Todo

- Add personalized_block/composer.json file - Anca
  IMPORTANT THIS IS NEEDED TO GET https://simplytest.me/ working
  - Just copying information from personalized_block.info.yml

- Write automated tests (See below)
  - Test demo install/uninstall
  - Test clientside JavaScript (personalized_block.js)
  - Test block rendering
    - .personalized-block > .personalized-block-content

--------------------------------------------------------------------------------

- Update project page on Drupal
  - Create a demo video using Bartik

Demo changes

- Add installation test to demo module - Anca

Module changes

- Write JavaScript Functional test
  - Confirm block is placed
  - Confirm query string parameter changes block content to variant.

# Later

- Create submodules. Not working as expected because the field storage
  is shared.
  - personalized_block_html.module
  - personalized_block_field.module
  - personalized_block_paragraph.module


- Optimize variant rendering by writing rendered output to files directory
  - Add data-file attribute to each variant.
  - Update JS to load file.
