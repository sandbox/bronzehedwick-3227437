<?php

namespace Drupal\Tests\personalized_block\Functional;

use Drupal\filter\Entity\FilterFormat;
use Drupal\Tests\BrowserTestBase;

/**
 * Test the Personalized Block module's installation via the Bartik theme.
 *
 * @group personalized_block
 */
class PersonalizedBlockBartikTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'bartik';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['personalized_block', 'personalized_block_demo'];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    FilterFormat::create([
      'format' => 'full_html',
      'name' => 'Full HTML',
    ])->save();

    FilterFormat::create([
      'format' => 'basic_html',
      'name' => 'Basic HTML',
    ])->save();
  }

  /**
   * Tests personalized_block functionality.
   */
  public function testPersonalizedBlockBasic() {
    $session = $this->assertSession();

    // Login to make sure the filters are available.
    $this->drupalLogin($this->rootUser);

    // Confirm optional bartik block example is placed on the homepage.
    $this->drupalGet('<front>');
    $session->responseContains('<div class="personalized-block-content" data-name="default">X</div>');
  }

}
