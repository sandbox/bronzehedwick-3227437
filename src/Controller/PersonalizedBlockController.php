<?php

namespace Drupal\personalized_block\Controller;

use Drupal\block_content\BlockContentInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller routines for personalized block routes.
 */
class PersonalizedBlockController extends ControllerBase {

  /**
   * Returns personalize admin page.
   *
   * @param \Drupal\block_content\BlockContentInterface $block_content
   *
   * @return string[]
   */
  public function personalize(BlockContentInterface $block_content) {
    $this->messenger()->addWarning('@todo Provide Redpoint integration and information on this page.');

    $build = [];
    // Info.
    $build['info'] = [
      '#type' => 'item',
      '#title' => $this->t('Personalization Description'),
      '#markup' => $block_content->label(),
    ];
    // ID.
    $build['id'] = [
      '#type' => 'item',
      '#title' => $this->t('Personalization ID'),
      '#markup' => $block_content->field_personalized_id->value,
    ];
    // Context.
    $terms = $block_content->field_personalized_context->referencedEntities();
    $term = $terms ? reset($terms) : NULL;
    $build['context'] = [
      '#type' => 'item',
      '#title' => $this->t('Personalization Context'),
      '#markup' => ($term) ? $term->label() : 'N/A',
    ];

    // Variants.
    $redirect_destination = $this->getRedirectDestination()->get();
    $redirect_uri = $redirect_destination
      ? 'base:' . str_replace(base_path(), '', $redirect_destination)
      : NULL;

    $block_content_types = personalized_block_get_types();
    $field_name = $block_content_types[$block_content->bundle()];
    $field = $block_content->$field_name;
    /** @var \Drupal\paragraphs\ParagraphInterface $paragraphs */
    $paragraphs = $field->referencedEntities();
    $items = [];
    foreach ($paragraphs as $paragraph) {
      $variant_id = $paragraph->field_personalized_id->value;
      if ($redirect_uri) {
        $items[] = [
          '#type' => 'link',
          '#title' => $variant_id,
          '#url' => Url::fromUri($redirect_uri, ['query' => ['_variant' => $variant_id]]),
        ];
      }
      else {
        $items[] = $variant_id;
      }
    }
    $build['variants'] = [
      '#type' => 'item',
      '#title' => $this->t('Personalization Variants'),
      'items' => [
        '#theme' => 'item_list',
        '#items' => $items,
      ],
    ];

    return $build;
  }

  /**
   * Returns personalize admin title.
   *
   * @param \Drupal\block_content\BlockContentInterface $block_content
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function personalizeTitle(BlockContentInterface $block_content) {
    return $block_content->label();
  }

  /**
   * Check personalized block access.
   */
  public static function checkPersonalizeAccess(BlockContentInterface $block_content) {
    $has_update_access = $block_content->access('update');

    $block_content_types = personalized_block_get_types();
    $is_personalized_block = isset($block_content_types[$block_content->bundle()]);

    return AccessResult::allowedIf($has_update_access && $is_personalized_block);
  }

  /* ************************************************************************ */

  /**
   * Returns personalized block test response.
   */
  public function test(Request $request) {
    $location = $request->query->get('location');
    $url = UrlHelper::parse($location);
    $variant_id = NestedArray::getValue($url, ['query', '_variant']);
    return JsonResponse::create(['variant_id' => $variant_id]);
  }

}
