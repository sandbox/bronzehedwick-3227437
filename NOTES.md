Export and import configuration
--------------------------------------

    # Personalized Block
    drush features-export -y personalized_block
    drush features-import -y personalized_block

    # Personalized Block Demo
    drush features-export -y personalized_block_demo
    drush features-import -y personalized_block_demo

Testing
-------

## Running PHPUnit tests.

@see [PHPUnit in Drupal](https://www.drupal.org/docs/automated-testing/phpunit-in-drupal)

    # Setup tests.
    export SIMPLETEST_DB=mysql://drupal:drupal@localhost/drupal;
    export SIMPLETEST_BASE_URL='http://localhost';
    export SYMFONY_DEPRECATIONS_HELPER='disabled';
    export BROWSERTEST_OUTPUT_DIRECTORY='DRUPAL_ROOT/sites/default/files/simpletest'
    pkill chromedriver
    chromedriver --port=4444 &

    # Execute tests.
    php ../../vendor/phpunit/phpunit/phpunit\
        --printer="\Drupal\Tests\Listeners\HtmlOutputPrinter"\
        {MODULE_PATH}

Review code
-----------

[PHP](https://www.drupal.org/node/1587138)

    # Check Drupal
    cd DRUPAL_ROOT
    phpcs --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info modules/sandbox/personalized_block

    # Check Drupal PHP best practices
    cd DRUPAL_ROOT
    phpcs --standard=DrupalPractice --extensions=php,module,inc,install,test,profile,theme,js,css,info modules/sandbox/personalized_block

[JavaScript](https://www.drupal.org/node/2873849)

    # Install Eslint. (One-time)
    cd DRUPAL_ROOT/core
    yarn install

    # Check Drupal JavaScript (ES5) legacy coding standards.
    cd DRUPAL_ROOT
    core/node_modules/.bin/eslint --no-eslintrc -c=core/.eslintrc.legacy.json --ext=.js modules/sandbox/personalized_block

[CSS](https://www.drupal.org/node/3041002)

    # Install Eslint. (One-time)
    cd DRUPAL_ROOT
    yarn install

    cd DRUPAL_ROOT
    yarn run lint:css ../modules/sandbox/personalized_block/css --fix
